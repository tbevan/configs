export ZSH=/home/tbevan/.oh-my-zsh

HOST=$(hostname -s)

# Base16 Shell
BASE16_SHELL="$HOME/.base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        eval "$("$BASE16_SHELL/profile_helper.sh")"

ZSH_THEME="fishy"
export DEFAULT_USER=tbevan

CASE_SENSITIVE="true"
HYPHEN_INSENSITIVE="true"
COMPLETION_WAITING_DOTS="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"
plugins=(git git-extras)

source $ZSH/oh-my-zsh.sh
source /home/tbevan/.zsh-syntax/zsh-syntax-highlighting.zsh

#ALIAS
alias ls="ls --color=auto"
alias ll="ls --color=auto -lh"
alias grep="grep --color=auto"
alias reload="source ~/.zshrc"
alias zsudo="sudo su -c 'ZDOTDIR=/home/tbevan zsh'"
alias vi="vim"
alias axel="axel -an 4"

#VARIABLES
export EDITOR=vim
export VISUAL=vim
export PATH=$HOME/.local/bin:/usr/local/bin:$PATH
export LD_LIBRARY_PATH=~/.local/lib:$LD_LIBRARY_PATH
export MANPATH=~/.local/share/man:$MANPATH


#System specific
if [ "$HOST" = "r2" ]; then
    alias q="squeue -a -o '%7i %.7P %.8j %.8u %.8Q %.2t %.10M %.5D %.4C %22N %22Y %r'"
    export PATH=/home/tbevan/anaconda3/bin:$PATH
fi

if [ "$HOST" = "r1-master" ]; then
    alias q="squeue -a -o '%7i %.7P %.8j %.8u %.8Q %.2t %.10M %.5D %.4C %22N %22Y %r'"
fi
