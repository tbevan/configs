call pathogen#infect()
set nocompatible

syntax on
filetype plugin indent on
nnoremap <silent> <Leader>/ :nohlsearch<CR>

" Put plugins and dictionaries in this dir (also on Windows)
let vimDir = '$HOME/.vim'
let &runtimepath.=','.vimDir

" Keep undo history across sessions by storing it in a file
if has('persistent_undo')
    let myUndoDir = expand(vimDir . '/undodir')
    " Create dirs
    call system('mkdir ' . vimDir)
    call system('mkdir ' . myUndoDir)
    let &undodir = myUndoDir
    set undofile
endif

" Uncomment the following to have Vim jump to the last position when                                                       
" reopening a file
if has("autocmd")
    au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
        \| exe "normal! g'\"" | endif
endif

set background=dark
set t_Co=256

"colorscheme monokai


colorscheme gruvbox
let g:gruvbox_italic = 1
"soft medium hard
let g:gruvbox_contrast_dark = 'hard'
let g:gruvbox_contrast_light = 'medium'

"colorscheme base16-default-dark

"set background=light
"colorscheme PaperColor

"colorscheme onehalfdark
"let g:airline_theme='onehalfdark'

set colorcolumn=120
set softtabstop=4 shiftwidth=4 expandtab 
set modeline
set exrc
set ignorecase smartcase
set incsearch
set nohlsearch
set showmatch
set shiftround
set autoindent
set copyindent
set nowrap
set backspace=indent,eol,start
set hidden
"set nobackup
"set noswapfile
nnoremap ; :


