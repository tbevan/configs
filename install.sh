#!/bin/bash
git submodule init
git submodule update
cp -r .vim ~/.vim
cp -r .oh-my-zsh ~/.oh-my-zsh
cp -r .zsh-syntax ~/.zsh-syntax
cp .vimrc ~/.vimrc
cp .zshrc ~/.zshrc

